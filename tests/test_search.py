import time
import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from page_objects.home import PaybrightHomePage
import allure
from selenium.webdriver.chrome.options import Options


@pytest.fixture
def browser():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    driver.maximize_window()
    yield driver
    driver.quit()


@pytest.mark.parametrize(
    "phrase", ["Samsung", "Apple", "SHEIN"],
)
@allure.title("This is parametrize test")
@allure.severity(allure.severity_level.TRIVIAL)
@allure.feature("Search Merchant on Homepage")
@allure.issue("https://paybright.atlassian.net/browse/PB-8643")
def test_merchant_search(browser, phrase: str):
    search_page = PaybrightHomePage(browser)
    search_page.load()
    search_page.search(phrase)
    time.sleep(2)
    if search_page.merchant_list_contains(phrase) == 'True':
        assert True
    else:
        time.sleep(2)
        allure.attach(browser.get_screenshot_as_png(), name="Screenshot", attachment_type=allure.attachment_type.PNG)
        assert False
